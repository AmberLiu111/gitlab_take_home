# Gitlab_take_home

## Layout

- [ ] [Multi tiers VPC] : 3 subnets with 2 security groups--
1 public subnet(ALB)+ 1 private subnet(webapp instance+ASG) + 1 private subnet(PostgreSQL instance)
- [ ] [EC2 with S3] : IAM role + EC2 + session manager+ bucket with object

- [ ] [EKS] : Eks cluster + node group + node(2 CPU, 8GB momery) with Roles
```
$ tree
.
├── main.tf
├── modules
│   ├── webapp
│   │   ├── cloud_config.yaml
│   │   ├── main.tf
│   │   ├── outputs.tf
│   │   └── variables.tf
│   ├── database
│   │   ├── main.tf
│   │   ├── outputs.tf
│   │   └── variables.tf
│   └── networking
│       ├── main.tf
│       ├── outputs.tf
│       └── variables.tf
│   └── eks
│       ├── main.tf
│       ├── outputs.tf
│       └── variables.tf
├── outputs.tf
├── myfiles
|  |── index.html
|  |── user_data.sh
├── terraform.tfvars
├── variables.tf
└── versions.tf
```

## Four modules

- [ ] [Networking](https://gitlab.com/AmberLiu111/gitlab_take_home/-/tree/main/modules/networking)
- [ ] [Database](https://gitlab.com/AmberLiu111/gitlab_take_home/-/tree/main/modules/database)
- [ ] [Webapp](https://gitlab.com/AmberLiu111/gitlab_take_home/-/tree/main/modules/webapp)
- [ ] [EKS](https://gitlab.com/AmberLiu111/gitlab_take_home/-/tree/main/modules/eks)

## Author
aiying liu
byrlay111@gmail.com

variable "region" {
  description = "AWS region"
  default     = "us-east-1"
  type        = string
}

variable "ssh_keypair" {
  description = "SSH keypair to use for EC2 instance"
  default     = null
  type        = string
}

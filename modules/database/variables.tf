variable "db_instance_identifier" {
  type = string
  default = "postgres"
}

variable "vpc" {
  type = any
}

variable "sg" {
  type = any
}

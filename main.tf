#module "database" {
#  source    = "./modules/database"
#  vpc       = module.networking.vpc
#  sg        = module.networking.sg
#
#}

module "networking" {
  source     = "./modules/networking" 
}

module "webapp" {
  source     = "./modules/webapp"
  ssh_keypair= var.ssh_keypair
  vpc        = module.networking.vpc
  sg         = module.networking.sg

}

module "eks" {
  source     = "./modules/eks"
  vpc        = module.networking.vpc
  sg         = module.networking.sg

}


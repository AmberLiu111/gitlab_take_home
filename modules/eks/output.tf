output "EKS_cluster" {
  description = "The EKS cluster"
  value       = aws_eks_cluster.my-eks.name
}

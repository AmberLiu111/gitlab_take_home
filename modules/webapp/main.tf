# Create a bucket
resource "aws_s3_bucket" "blog" {

  bucket = "s3-terraform-bucket-lab-1019"

  acl    = "private"   # or can be "public-read"

  tags = {

    Name        = "My bucket"

  }

}

# Upload an object
resource "aws_s3_bucket_object" "object" {

  bucket = aws_s3_bucket.blog.id

  key    = "profile"

  acl    = "private"

  source = "myfiles/index.html"

  etag = filemd5("myfiles/index.html")

}

# IAM Role for EC2
resource "aws_iam_role" "SSMRoleForEC2" {
  name               = "SSMRoleForEC2"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_instance_profile" "SSMRoleForEC2" {
  name = "SSMRoleForEC2"
  role = aws_iam_role.SSMRoleForEC2.name
}

resource "aws_iam_role_policy_attachment" "role-policy-attachment" {
  for_each = toset([
    "arn:aws:iam::aws:policy/AmazonEC2FullAccess",
    "arn:aws:iam::aws:policy/AmazonS3FullAccess"
  ])

  role       = aws_iam_role.SSMRoleForEC2.name
  policy_arn = each.value
}

# EC2 instance image
data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
  owners = ["099720109477"]
}


# EC2 launch template used for ASG
resource "aws_launch_template" "webserver" {
  image_id      = data.aws_ami.ubuntu.id
  instance_type = "t2.micro"
  user_data     = filebase64("myfiles/user_data.sh")
  key_name               = var.ssh_keypair
  vpc_security_group_ids = [var.sg.websvr]
  iam_instance_profile  {
    name = aws_iam_instance_profile.SSMRoleForEC2.name
  }
}

# Create Auto scaling group
resource "aws_autoscaling_group" "webserver" {
  name                = "my-asg"
  min_size            = 1
  max_size            = 3
  vpc_zone_identifier = var.vpc.private_subnets
  target_group_arns   = module.alb.target_group_arns
  launch_template {
    id      = aws_launch_template.webserver.id
  }
  #launch_configuration= aws_launch_template.webserver.name
}

# Create loan balancer
module "alb" {
  source             = "terraform-aws-modules/alb/aws"
  version            = "~> 5.0"
  load_balancer_type = "application"
  vpc_id             = var.vpc.vpc_id
  subnets            = var.vpc.public_subnets
  security_groups    = [var.sg.lb]

  http_tcp_listeners = [
    {
      port               = 80
      protocol           = "HTTP"
      target_group_index = 0
    }
  ]

  target_groups = [
    { name_prefix      = "websvr"
      backend_protocol = "HTTP"
      backend_port     = 8080
      target_type      = "instance"
    }
  ]
}

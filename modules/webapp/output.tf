# S3 Outputs
output "S3_bucket" {
  description = "The s3 bucket id"
  value       = aws_s3_bucket.blog.id
}

output "EC2_image_id" {
  description = "The EC2 image id"
  value       = data.aws_ami.ubuntu.id
}

output "launch_template" {
  description = "The ASG launch template id"
  value       = aws_launch_template.webserver.id
}

